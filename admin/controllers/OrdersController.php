<?php

namespace admin\controllers;

use admin\models\OrderItem;
use admin\models\OrderSearch;
use admin\models\Order;
use common\models\OrderStatus;
use Yii;
use yii\db\IntegrityException;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * OrdersController implements the CRUD actions for Order model.
 */
class OrdersController extends _BaseController
{
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->edit();
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        return $this->edit($id);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null && $model->deleted == false) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function edit($id = null)
    {
        $model = is_null($id) ? new Order() : $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->createdAt = !empty($model->createdAt) ? $model->createdAt : date('Y-m-d H:i:s');
            $model->updatedAt = date('Y-m-d H:i:s');
            if ($model->save()) {
                $this->saveOrderItems($model);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'item' => new OrderItem(['orderId' => $id]),
            'statuses' => OrderStatus::find()->where(['deleted' => false])->asArray()->all()
        ]);
    }

    private function saveOrderItems(&$model)
    {
        /** @var $model Order */
        $orderItems = Yii::$app->request->post('OrderItem', []);
        $modelItemsIds = ArrayHelper::getColumn($model->orderItems, 'id');

        // Sort items. Entries with empty values and unknown ID will be ignored
        $newItems = [];
        $existItems = [];
        foreach ($orderItems as &$item) {
            if (isset($item['id'])) {
                if (!empty($item['id']) && in_array($item['id'], $modelItemsIds) && !empty($item['productId'])) {
                    $existItems [] = [
                        'id' => $item['id'],
                        'orderId' => $model->id,
                        'productId' => $item['productId'],
                        'productPrice' => $item['productPrice'],
                        'quantity' => $item['quantity'] ? $item['quantity'] : 1,
                        'discount' => $item['discount'] ? $item['discount'] : 0,
                    ];
                    unset($modelItemsIds[array_search($item['id'], $modelItemsIds)]);
                }
            } else {
                if (!empty($item['productId']) && !empty($item['quantity'])) {
                    $newItems [] = [
                        'orderId' => $model->id,
                        'productId' => $item['productId'],
                        'productPrice' => $item['productPrice'],
                        'quantity' => $item['quantity'] ? $item['quantity'] : 1,
                        'discount' => $item['discount'] ? $item['discount'] : 0,
                    ];
                }
            }
        }

        // Remove old order items
        if (!empty($modelItemsIds)) {
            OrderItem::deleteAll(['id' => $modelItemsIds]);
        }

        // Update exist order items
        if (!empty($existItems)) {
            $updateRows = OrderItem::getDb()->createCommand()->batchInsert(
                OrderItem::tableName(),
                ['id', 'orderId', 'productId', 'productPrice', 'quantity', 'discount'],
                $existItems
            );
            if (OrderItem::getDb()->driverName == 'pgsql') {
                $updateRows->rawSql .= ' ON CONFLICT ("id") DO UPDATE SET
                "quantity"=excluded."quantity",
                "discount"=excluded."discount"
                ';
                // TODO: implement other fields updating
            }
            if (OrderItem::getDb()->driverName == 'mysql') {
                $updateRows->rawSql .= ' ON DUPLICATE KEY UPDATE id=id';
            }
            try {
                $updateRows->execute();
            } catch (IntegrityException $exception) {
                throw $exception;
                // No operation. Just ignore it
            }
        }

        // Add new order items
        if (!empty($newItems)) {
            $newRows = OrderItem::getDb()->createCommand()->batchInsert(
                OrderItem::tableName(), ['orderId', 'productId', 'productPrice', 'quantity', 'discount'], $newItems
            );
            if (OrderItem::getDb()->driverName == 'pgsql') {
                $newRows->rawSql .= ' ON CONFLICT ("id") DO NOTHING';
            }
            if (OrderItem::getDb()->driverName == 'mysql') {
                $newRows->rawSql .= ' ON DUPLICATE KEY UPDATE id=id';
            }
            $newRows->execute();
        }
        return true;

    }
}
