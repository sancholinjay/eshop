<?php

namespace admin\controllers;

use admin\models\Category;
use admin\models\Characteristic;
use admin\models\forms\ProductImageForm;
use admin\models\Product;
use admin\models\ProductCharacteristic;
use admin\models\ProductSearch;
use common\models\Brand;
use common\models\ProductImage;
use Yii;
use Yii\db\IntegrityException;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Product model.
 */
class ProductsController extends _BaseController
{
    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $categoriesExist = Category::find()->where(['deleted' => false])->count();
        $characteristicExist = Characteristic::find()->where(['deleted' => false])->count();
        $brandsExist = Brand::find()->where(['deleted' => false])->count();

        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'brandsExist' => $brandsExist,
            'categoriesExist' => $categoriesExist,
            'characteristicExist' => $characteristicExist,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->edit();
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        return $this->edit($id);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Get the list of Characteristic specified for selected category.
     * Returns list with ID and Name for each characteristic as JSON object.
     * @param $categoryId
     * @return mixed
     */
    public function actionGetCharacteristicList($categoryId)
    {
        $characteristics = Characteristic::find()
            ->select(['id', 'name'])
            ->where(['deleted' => false, 'categoryId' => $categoryId])
            ->orderBy('name')
            ->asArray()
            ->all();
        array_unshift($characteristics, ['id' => null, 'name' => 'Выберите характеристику']);

        return $this->asJson($characteristics);
    }


    private function edit($id = null)
    {
        $model = is_null($id) ? new Product() : $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->updatedAt = date('Y-m-d H:i:s');
            if ($model->save()) {
                $this->saveProductCharacteristics($model);
                $this->saveProductImages($model);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $brands = Brand::find()
            ->select(['id', 'name'])
            ->where(['or',
                ['deleted' => false],
                ['id' => $model->brandId],
            ])->orderBy('name')
            ->asArray()->all();
        $brands = ArrayHelper::map($brands, 'id', 'name');

        $categories = Category::find()
            ->select(['id', 'name'])
            ->where(['or',
                ['deleted' => false],
                ['id' => $model->categoryId],
            ])->orderBy('name')
            ->asArray()->all();
        $categories = ArrayHelper::map($categories, 'id', 'name');

        $characteristics = [new ProductCharacteristic()];
        $characteristics += $model->productCharacteristics;

        $imageForm = new ProductImageForm();

        return $this->render('edit', [
            'model' => $model,
            'brands' => $brands,
            'categories' => $categories,
            'characteristics' => $characteristics,
            'imageForm' => $imageForm,
        ]);

    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null && $model->deleted == false) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function saveProductCharacteristics(&$model)
    {
        /** @var $model Product */
        $characteristics = Yii::$app->request->post('ProductCharacteristic', []);
        $modelCharacteristicIds = ArrayHelper::getColumn($model->productCharacteristics, 'id');

        // Sort characteristics. Entries with empty values and unknown ID will be ignored
        $newCharacteristics = [];
        $existCharacteristics = [];
        foreach ($characteristics as &$characteristic) {
            if (isset($characteristic['id'])) {
                if (!empty($characteristic['characteristicId'])
                    && !empty($characteristic['value'])
                    && in_array($characteristic['id'], $modelCharacteristicIds)
                ) {
                    $existCharacteristics [] = [
                        'id' => $characteristic['id'],
                        'productId' => $model->id,
                        'characteristicId' => $characteristic['characteristicId'],
                        'value' => $characteristic['value'],
                    ];
                    unset($modelCharacteristicIds[array_search($characteristic['id'], $modelCharacteristicIds)]);
                }
            } else {
                if (!empty($characteristic['characteristicId']) && !empty($characteristic['value'])) {
                    $newCharacteristics [] = [
                        'productId' => $model->id,
                        'characteristicId' => $characteristic['characteristicId'],
                        'value' => $characteristic['value'],
                    ];
                }
            }
        }

        // Remove old characteristics
        if (!empty($modelCharacteristicIds)) {
            ProductCharacteristic::deleteAll(['id' => $modelCharacteristicIds]);
        }

        // Update exist characteristics
        if (!empty($existCharacteristics)) {
            $updateRows = ProductCharacteristic::getDb()->createCommand()->batchInsert(
                ProductCharacteristic::tableName(),
                ['id', 'productId', 'characteristicId', 'value'],
                $existCharacteristics
            );
            if (ProductCharacteristic::getDb()->driverName == 'pgsql') {
                $updateRows->rawSql .= ' ON CONFLICT ("id") DO UPDATE SET
                "productId"=excluded."productId",
                "characteristicId"=excluded."characteristicId",
                "value"=excluded."value"
                ';
            }
            if (ProductCharacteristic::getDb()->driverName == 'mysql') {
                $updateRows->rawSql .= ' ON DUPLICATE KEY UPDATE id=id';
            }
            try {
                $updateRows->execute();
            } catch (IntegrityException $exception) {
                // No operation. Just ignore it
            }
        }

        // Add new characteristics
        if (!empty($newCharacteristics)) {
            $newRows = ProductCharacteristic::getDb()->createCommand()->batchInsert(
                ProductCharacteristic::tableName(), ['productId', 'characteristicId', 'value'], $newCharacteristics
            );
            if (ProductCharacteristic::getDb()->driverName == 'pgsql') {
                $newRows->rawSql .= ' ON CONFLICT ("productId", "characteristicId", "value") DO NOTHING';
            }
            if (ProductCharacteristic::getDb()->driverName == 'mysql') {
                $newRows->rawSql .= ' ON DUPLICATE KEY UPDATE id=id';
            }
            $newRows->execute();
        }
        return true;
    }

    private function saveProductImages(&$model)
    {
        /** @var $model Product */
        $images = Yii::$app->request->post('ProductImage', []);
        $modelImagesIds = ArrayHelper::getColumn($model->productImages, 'id');

        // Sort images
        foreach ($images as &$image) {
            if (isset($image['id'])) {
                unset($modelImagesIds[array_search($image['id'], $modelImagesIds)]);
            }
        }

        // Remove old images
        $modelImages = ProductImage::findAll(['id' => $modelImagesIds]);
        foreach ($modelImages as $image) {
            @unlink(Yii::getAlias('@shop') . "/web{$image->imageUrl}");
            $image->delete();
        }
        unset($modelImagesIds, $images);

        // Save new images
        $uploadForm = new ProductImageForm();
        $uploadForm->image = UploadedFile::getInstances($uploadForm, 'image');
        if ($uploadForm->image && $model->validate()) {
            foreach ($uploadForm->image as $file) {
                $fileId = crc32($model->id . microtime(true));
                $url = "/uploads/product-pics/{$fileId}.{$file->extension}";
                $file->saveAs(Yii::getAlias('@shop') . "/web{$url}");

                $newImage = new ProductImage([
                    'productId' => $model->id,
                    'imageUrl' => $url,
                ]);
                $newImage->save();
            }
        } else {
            return false;
        }
        return true;
    }
}
