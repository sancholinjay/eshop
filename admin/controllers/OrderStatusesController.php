<?php

namespace admin\controllers;

use admin\models\OrderStatusSearch;
use common\models\OrderStatus;
use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * OrderStatusesController implements the CRUD actions for OrderStatus model.
 */
class OrderStatusesController extends _BaseController
{
    /**
     * Lists all OrderStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderStatus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrderStatus model.
     * Sends status code 200 if creation is successful.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->edit();
    }

    /**
     * Updates an existing OrderStatus model.
     * Sends status code 200 if update is successful.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        return $this->edit($id);
    }

    /**
     * Deletes an existing OrderStatus model.
     * Sends status code 200 if deletion is successful.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        $model = $this->findModel($id);
        $deletable = !$model->hasAttribute('deleted');
        if (!$model->delete()) {
            throw new ServerErrorHttpException(Json::encode($model->errors));
        }
        return $this->asJson([
            'state' => 'success',
            'message' => $deletable ? 'Запись архивирована' : 'Запись удалена',
        ]);
    }


    /**
     * Creates a new OrderStatus model or Updates an existing OrderStatus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function edit($id = null)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        $model = is_null($id) ? new OrderStatus() : $this->findModel($id);
        $isNewRecord = $model->isNewRecord;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson([
                'state' => 'success',
                'message' => $isNewRecord ? 'Запись добавлена' : 'Запись обновлена',
            ]);
        }

        return $this->renderAjax('edit', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the OrderStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderStatus::findOne($id)) !== null) {
            if (!$model->deleted) {
                return $model;
            }
        }

        throw new NotFoundHttpException();
    }
}
