<?php

namespace admin\controllers;

use Yii;
use common\models\Brand;
use admin\models\BrandSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ServerErrorHttpException;

/**
 * BrandsController implements the CRUD actions for Brand model.
 */
class BrandsController extends _BaseController
{
    /**
     * Lists all Brand models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brand model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brand model.
     * Sends status code 200 if creation is successful.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->edit();
    }

    /**
     * Updates an existing Brand model.
     * Sends status code 200 if update is successful.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        return $this->edit($id);
    }

    /**
     * Deletes an existing Brand model.
     * Sends status code 200 if deletion is successful.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        $model = $this->findModel($id);
        $deletable = !$model->hasAttribute('deleted');
        if (!$model->delete()) {
            throw new ServerErrorHttpException(Json::encode($model->errors));
        }
        return $this->asJson([
            'state' => 'success',
            'message' => $deletable ? 'Запись архивирована' : 'Запись удалена',
        ]);
    }


    /**
     * Creates a new Brand model or Updates an existing Brand model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function edit($id = null)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        $model = is_null($id) ? new Brand() : $this->findModel($id);
        $isNewRecord = $model->isNewRecord;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson([
                'state' => 'success',
                'message' => $isNewRecord ? 'Запись добавлена' : 'Запись обновлена',
            ]);
        }

        return $this->renderAjax('edit', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brand the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brand::findOne($id)) !== null) {
            if (!$model->hasAttribute('deleted') || ($model->hasAttribute('deleted') && !$model->deleted)) {
                return $model;
            }
        }

        throw new NotFoundHttpException();
    }
}
