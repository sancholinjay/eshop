<?php

namespace admin\controllers;

use admin\models\ModificationSearch;
use common\models\Category;
use common\models\Modification;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * ModificationsController implements the CRUD actions for Modification model.
 */
class ModificationsController extends _BaseController
{
    /**
     * Lists all Modification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $categoriesExist = Category::find()->where(['deleted' => false])->count();
        $searchModel = new ModificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'categoriesExist' => boolval($categoriesExist),
        ]);
    }

    /**
     * Displays a single Modification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Modification model.
     * Sends status code 200 if creation is successful.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->edit();
    }

    /**
     * Updates an existing Modification model.
     * Sends status code 200 if update is successful.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        return $this->edit($id);
    }

    /**
     * Deletes an existing Modification model.
     * Sends status code 200 if deletion is successful.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        $model = $this->findModel($id);
        $deletable = !$model->hasAttribute('deleted');
        if (!$model->delete()) {
            throw new ServerErrorHttpException(Json::encode($model->errors));
        }
        return $this->asJson([
            'state' => 'success',
            'message' => $deletable ? 'Запись архивирована' : 'Запись удалена',
        ]);
    }


    /**
     * Creates a new Modification model or Updates an existing Modification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function edit($id = null)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        $model = is_null($id) ? new Modification() : $this->findModel($id);
        $isNewRecord = $model->isNewRecord;

        $categories = ArrayHelper::map(Category::findAll(['deleted' => false]), 'id', 'name');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson([
                'state' => 'success',
                'message' => $isNewRecord ? 'Запись добавлена' : 'Запись обновлена',
            ]);
        }

        return $this->renderAjax('edit', [
            'model' => $model,
            'categories' => $categories
        ]);
    }


    /**
     * Finds the Modification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Modification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Modification::findOne($id)) !== null) {
            if (!$model->deleted) {
                return $model;
            }
        }

        throw new NotFoundHttpException();
    }
}
