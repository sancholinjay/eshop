<?php

namespace admin\components;


use Yii;
use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $header = '<span class="fa fa-cogs"></span>';
    public $renderHtmlButtons = false;

    public $headerOptions = ['style' => 'width: 25px;'];

    public function init()
    {
        $this->template = Html::tag('div', $this->template, [
            'class' => $this->renderHtmlButtons ? 'btn-group' : 'btn-group btn-group-justified',
            'style' => $this->renderHtmlButtons ? 'display:inline-flex' : 'display:inline-block',
        ]);

        $this->initDefaultButtons();
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        $this->initDefaultButton('view', 'eye');
        $this->initDefaultButton('update', 'edit');
        $this->initDefaultButton('delete', 'trash', [
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ]);
    }

    /**
     * Initializes the default button rendering callback for single button.
     * @param string $name Button name as it's written in template
     * @param string $iconName The part of Fon Awesome icon class that makes it unique
     * @param array $additionalOptions Array of additional options
     * @since 2.0.11
     */
    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        $class = 'btn-success action-view';
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        $class = 'btn-info action-update';
                        break;
                    case 'delete':
                        $title = Yii::t('yii', 'Delete');
                        $class = 'btn-danger action-delete';
                        $dataPjax = $this->renderHtmlButtons ? 1 : 0;
                        break;
                    default:
                        $title = ucfirst($name);
                        $class = 'btn-default';
                }
                $options = array_merge([
                    'title' => $title,
                    'class' => "btn btn-xs {$class}",
                    'aria-label' => $title,
                    'data-pjax' => isset($dataPjax) ? $dataPjax : 0,
                ], $additionalOptions, $this->buttonOptions);
                $icon = Html::tag('span', '', ['class' => "fa fa-$iconName"]);
                return $this->renderHtmlButtons
                    ? Html::button($icon, $options + ['data-url' => $url])
                    : Html::a($icon, $url, $options);
            };
        }
    }
}
