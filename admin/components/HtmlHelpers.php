<?php


namespace admin\components;


use yii\bootstrap\Modal;
use yii\helpers\Html;

class HtmlHelpers
{
    public static function renderSubmitButton()
    {
        return join("\n", [
            Html::beginTag('div', ['class' => 'form-group']),
            Html::submitButton('<span class="fa fa-save"></span> Сохранить', ['class' => 'btn btn-success']),
            Html::endTag('div')
        ]);
    }


    public static function renderEditorModal(): string
    {
        return Modal::widget([
            'id' => 'editor-modal',
            'header' => '<h4 class="no-margin"> </h4>',
            'headerOptions' => [
                'class' => 'panel-heading',
                'style' => 'padding: 10px'
            ],
            'closeButton' => [
                'class' => 'btn btn-xs btn-danger',
                'label' => '<span class="fa fa-times"></span>',
                'style' => 'position:absolute; right:15px',
            ],
            'clientOptions' => ['backdrop' => 'static'],
            'size' => Modal::SIZE_DEFAULT,
        ]);
    }
}