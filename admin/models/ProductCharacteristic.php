<?php


namespace admin\models;


use common\models\Characteristic;
use common\models\Product;

class ProductCharacteristic extends \common\models\ProductCharacteristic
{
    public function rules()
    {
        return [
            [['productId', 'characteristicId'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['productId', 'characteristicId', 'value'], 'unique', 'targetAttribute' => ['productId', 'characteristicId', 'value']],
            [['characteristicId'], 'exist', 'skipOnError' => true, 'targetClass' => Characteristic::className(), 'targetAttribute' => ['characteristicId' => 'id']],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

}