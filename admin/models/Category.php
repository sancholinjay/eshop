<?php


namespace admin\models;


/**
 * @property mixed $parent
 */
class Category extends \common\models\Category
{
    public $parentCategory;

    public function getParent()
    {
        return $this->hasOne(Category::class, ['id' => 'parentId']);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), ['parentCategory' => 'Родительская категория']);
    }
}
