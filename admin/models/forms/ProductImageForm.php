<?php


namespace admin\models\forms;


use yii\base\Model;

class ProductImageForm extends Model
{
    public $id;
    public $image;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['image'], 'file', 'maxSize' => 1024000,
                'extensions' => ['jpg', 'png'],
                'checkExtensionByMimeType' => true,
                'mimeTypes' => ['image/jpeg', 'image/png'],
                'wrongMimeType' => 'Недопустимый тип файла'
            ],
        ];
    }

    public function attributeLabels()
    {
        return ['image' => 'Изображение'];
    }

}