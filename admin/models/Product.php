<?php


namespace admin\models;


use common\behaviors\DateTimeBehavior;

/**
 *
 * @property string $imageUrl
 */
class Product extends \common\models\Product
{
    const NO_IMAGE = '/img/product-image-default.png';

    public function behaviors()
    {
        return [
            DateTimeBehavior::class,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCharacteristics()
    {
        return $this->hasMany(ProductCharacteristic::className(), ['productId' => 'id']);
    }

    public function getImageUrl()
    {
        if (is_array($this->productImages) && !empty($this->productImages)) {
            return $this->productImages[0]->imageUrl;
        } else {
            return self::NO_IMAGE;
        }
    }
}
