<?php


namespace admin\models;


class Characteristic extends \common\models\Characteristic
{
    public $categoryName;

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), ['categoryName' => 'Категория']);
    }

}
