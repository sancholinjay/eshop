<?php


namespace admin\models;


use common\models\Order;
use common\models\Product;

class OrderItem extends \common\models\OrderItem
{
    public $amount;

    public function rules()
    {
        return [
            [['quantity', 'productPrice'], 'required'],
            [['orderId', 'productId'], 'integer'],
            [['productPrice', 'discount', 'amount'], 'number'],
            [['quantity'], 'integer', 'min' => 1],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['orderId' => 'id']],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

}
