<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CategorySearch represents the model behind the search form of `admin\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parentId'], 'integer'],
            [['name', 'parentCategory'], 'safe'],
            [['deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find()->alias('category')
            ->where(['category.deleted' => false])
            ->joinWith(['parent parent']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]],
        ]);
        $dataProvider->sort->attributes += [
            'parentCategory' => [
                'asc' => ['parent.name' => SORT_ASC],
                'desc' => ['parent.name' => SORT_DESC],
            ],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'category.id' => $this->id,
        ]);

        $query->andFilterWhere(['ilike', 'category.name', $this->name]);
        $query->andFilterWhere(['ilike', 'parent.name', $this->parentCategory]);

        return $dataProvider;
    }
}
