<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CharacteristicSearch represents the model behind the search form of `admin\models\Characteristic`.
 */
class CharacteristicSearch extends Characteristic
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'categoryName'], 'safe'],
            [['deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Characteristic::find()->alias('characteristic')
            ->where(['characteristic.deleted' => false])
            ->joinWith('category category');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes += [
            'categoryName' => [
                'asc' => ['category.name' => SORT_ASC],
                'desc' => ['category.name' => SORT_DESC],
            ],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'characteristic.id' => $this->id,
        ]);

        $query->andFilterWhere(['ilike', 'characteristic.name', $this->name]);
        $query->andFilterWhere(['ilike', 'category.name', $this->categoryName]);

        return $dataProvider;
    }
}
