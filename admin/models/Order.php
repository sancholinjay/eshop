<?php


namespace admin\models;


class Order extends \common\models\Order
{
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['orderId' => 'id']);
    }
}
