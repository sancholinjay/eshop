<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'phoneNumber', 'status'], 'integer'],
            [['username', 'email', 'passwordHash', 'passwordResetToken', 'verificationToken', 'authKey', 'lastName', 'firstName', 'profileImgUrl', 'createdAt', 'updatedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'phoneNumber' => $this->phoneNumber,
            'status' => $this->status,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ]);

        $query->andFilterWhere(['ilike', 'username', $this->username])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'passwordHash', $this->passwordHash])
            ->andFilterWhere(['ilike', 'passwordResetToken', $this->passwordResetToken])
            ->andFilterWhere(['ilike', 'verificationToken', $this->verificationToken])
            ->andFilterWhere(['ilike', 'authKey', $this->authKey])
            ->andFilterWhere(['ilike', 'lastName', $this->lastName])
            ->andFilterWhere(['ilike', 'firstName', $this->firstName])
            ->andFilterWhere(['ilike', 'profileImgUrl', $this->profileImgUrl]);

        return $dataProvider;
    }
}
