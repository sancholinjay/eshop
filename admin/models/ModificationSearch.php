<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ModificationSearch represents the model behind the search form of `admin\models\Modification`.
 */
class ModificationSearch extends Modification
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'categoryName', 'dataType'], 'safe'],
            [['deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Modification::find()->alias('Modification')
            ->where(['Modification.deleted' => false])
            ->joinWith('category category');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes += [
            'categoryName' => [
                'asc' => ['category.name' => SORT_ASC],
                'desc' => ['category.name' => SORT_DESC],
            ],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Modification.id' => $this->id,
            'Modification.dataType' => $this->dataType,
        ]);

        $query->andFilterWhere(['ilike', 'Modification.name', $this->name]);
        $query->andFilterWhere(['ilike', 'category.name', $this->categoryName]);

        return $dataProvider;
    }
}
