<?php


namespace admin\models;


/**
 */
class Modification extends \common\models\Modification
{
    const TYPE_STRING = 1;
    const TYPE_NUMBER = 2;
    const TYPE_COLOR = 3;

    const TYPES = [
        self::TYPE_STRING => 'Строка',
        self::TYPE_NUMBER => 'Число',
        self::TYPE_COLOR => 'Цвет',
    ];

    public $categoryName;

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), ['categoryName' => 'Категория']);
    }

}
