<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Modification */
/* @var $categories array */

$this->title = $model->isNewRecord ? "Новый модификатор" : "Изменить модификатор: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Модификаторы товаров', 'url' => ['index']];
?>
<div class="modifications-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>
</div>
