<?php

use admin\components\ActionColumn;
use admin\components\HtmlHelpers;
use admin\models\Modification;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\ModificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categoriesExist bool */

$this->title = 'Модификаторы товаров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modification-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($categoriesExist): ?>
        <?= Html::button('Добавить модификатор', [
            'class' => 'btn btn-success action-create',
            'data-url' => Url::to(['create']),
        ]) ?>
    <?php else: ?>
        <?= Alert::widget([
            'options' => [
                'class' => 'alert-danger alert-dismissible',
            ],
            'body' => '<h4><i class="icon fa fa-ban"></i> Модификаторы товаров недоступны</h4>' .
                'Для управления модификаторами необходимо добавить хотя бы одну ' .
                Html::a('Категорию товаров', ['/categories']),
        ]) ?>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'modification-pjax', 'timeout' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 50px'],
            ],
            'name',
            [
                'attribute' => 'categoryName',
                'value' => 'category.name',
            ],
            [
                'attribute' => 'dataType',
                'value' => function ($model) {
                    return isset($model::TYPES[$model->dataType]) ? $model::TYPES[$model->dataType] : 'Нет';
                },
            ],
            [
                'class' => ActionColumn::class,
                'headerOptions' => ['style' => 'width: 64px'],
                'renderHtmlButtons' => true,
                'visibleButtons' => [
                    'view' => false,
                    'update' => $categoriesExist,
                    'delete' => $categoriesExist,
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    <?= HtmlHelpers::renderEditorModal() ?>
</div>
