<?php

use admin\components\HtmlHelpers;
use admin\models\Modification;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\Modification */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories array */
?>

<?php $form = ActiveForm::begin(['id' => 'modifications-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'dataType')->dropDownList(Modification::TYPES) ?>
<?= $form->field($model, 'categoryId')->dropDownList($categories, ['prompt' => 'Выберите категорию']) ?>
<?= HtmlHelpers::renderSubmitButton() ?>
<?php ActiveForm::end(); ?>
