<?php

/* @var $this yii\web\View */

$this->title = 'My Application';

use yii\helpers\Html; ?>
<div class="site-index">

    <div class="btn-group">
        <?= Html::a('<span class="fa fa-shopping-cart"></span> Товары', ['/products'], [
            'class' => 'btn btn-primary'
        ]) ?>
        <?= Html::a('<span class="fa fa-shopping-cart"></span> Категории', ['/categories'], [
            'class' => 'btn btn-primary'
        ]) ?>
        <?= Html::a('<span class="fa fa-shopping-cart"></span> Характеристики', ['/characteristics'], [
            'class' => 'btn btn-primary'
        ]) ?>
        <?= Html::a('<span class="fa fa-shopping-cart"></span> Модификаторы', ['/modifications'], [
            'class' => 'btn btn-primary'
        ]) ?>
        <?= Html::a('<span class="fa fa-shopping-cart"></span> Бренды', ['/brands'], [
            'class' => 'btn btn-primary'
        ]) ?>
        <?= Html::a('<span class="fa fa-credit-card"></span> Заказы', ['/orders'], [
            'class' => 'btn btn-primary'
        ]) ?>
        <?= Html::a('<span class="fa fa-shopping-cart"></span> Статусы заказа', ['/order-statuses'], [
            'class' => 'btn btn-primary'
        ]) ?>
        <?= Html::a('<span class="fa fa-shopping-cart"></span> Инвентарь (недоступно)', ['/inventory'], [
            'class' => 'btn btn-primary'
        ]) ?>
    </div>
</div>
