<?php

use admin\components\HtmlHelpers;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'order-status-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<div class="row">
    <div class="col-xs-10">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'autofocus' => true]) ?></div>
    <div class="col-xs-2">
        <?= $form->field($model, 'color')->input('color') ?></div>
</div>
<?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
<?= HtmlHelpers::renderSubmitButton() ?>
<?php ActiveForm::end(); ?>
