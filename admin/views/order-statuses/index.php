<?php

use admin\components\ActionColumn;
use admin\components\HtmlHelpers;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\OrderStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статусы заказа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::button('Добавить статус', [
        'class' => 'btn btn-success action-create',
        'data-url' => Url::to(['create']),
    ]) ?>

    <?php Pjax::begin(['id' => 'order-status-pjax', 'timeout' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 50px'],
            ],
            'name',
            'color',
            'description',
            [
                'class' => ActionColumn::class,
                'headerOptions' => ['style' => 'width: 64px'],
                'renderHtmlButtons' => true,
                'visibleButtons' => [
                    'view' => false,
                    'update' => true,
                    'delete' => true,
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    <?= HtmlHelpers::renderEditorModal() ?>
</div>
