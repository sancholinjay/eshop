<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderStatus */

$this->title = $model->isNewRecord ? "Новый статус заказа" : "Изменить статус заказа: " . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Статусы заказа', 'url' => ['index']];
?>
<div class="order-status-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
