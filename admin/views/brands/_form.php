<?php

use admin\components\HtmlHelpers;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Brand */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'brands-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
<?= HtmlHelpers::renderSubmitButton() ?>
<?php ActiveForm::end(); ?>