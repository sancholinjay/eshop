<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Brand */

$this->title = $model->isNewRecord ? "Новый бренд" : "Редактирование бренда: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => ['index']];
?>
<div class="brand-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
