<?php

use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
$enableManagement = true;
?>
<div class="order-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($enableManagement): ?>
        <?= Html::a('Новый заказ', ['create'], ['class' => 'btn btn-success']) ?>
    <?php else: ?>
        <?= Alert::widget([
            'options' => [
                'class' => 'alert-danger alert-dismissible',
            ],
            'body' => '<h4><i class="icon fa fa-ban"></i> Управление заказами недоступно</h4>' .
                join("<br>", [
                    'Для управления товарами требуется сначала добавить:',
                    '* ' . Html::a('Товары', ['/brands']),
                ]),
        ]) ?>
    <?php endif; ?>
    <?php Pjax::begin(['id' => 'orders-pjax', 'timeout' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'userId',
            'status',
            'amount',
            'addressState',
            'addressRegion',
            'addressLocality',
            'addressStreet',
            'addressHouse',
            'addressApartment',
            'createdAt',
            'updatedAt',
//            'deleted:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
