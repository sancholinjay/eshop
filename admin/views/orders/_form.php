<?php

use admin\assets\AppAsset;
use admin\components\HtmlHelpers;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $item admin\models\OrderItem */
/* @var $statuses array */

$this->registerJsFile('/js/forms/orders.js', [
    'position' => View::POS_END,
    'depends' => AppAsset::class,
]);

$statuses = ArrayHelper::map($statuses, 'id', 'name');
?>

<?php $form = ActiveForm::begin(['id' => 'characteristic-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<div class="row">
    <fieldset class="col-xs-12">
        <legend class="h4">Информация о заказе</legend>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'userId')->dropDownList(['1' => '1']) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'status')->dropDownList($statuses, [
                    'prompt' => 'Выберите статус заказа'
                ]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'createdAt')->textInput() ?>
            </div>
        </div>
    </fieldset>
</div>
<div class="row">
    <fieldset class="col-xs-12">
        <legend class="h4">Адрес доставки</legend>
        <div class="row row-space-6">
            <div class="col-sm-2">
                <?= $form->field($model, 'addressState')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'addressRegion')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'addressLocality')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'addressStreet')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-1">
                <?= $form->field($model, 'addressHouse')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-1">
                <?= $form->field($model, 'addressApartment')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </fieldset>
</div>
<div class="row">
    <fieldset class="col-xs-12 table-responsive">
        <legend class="h4">Содержимое заказа
            <?= Html::button('<span class="fa fa-plus"></span> Добавить товар', [
                'class' => 'btn btn-sm btn-primary pull-right btn-add-item',
            ]) ?>
        </legend>
        <table class="table table-condensed items-table">
            <thead>
            <tr>
                <th style="width: 50px">Товар</th>
                <th style="width: 60%"></th>
                <th style="width: 10%">Цена</th>
                <th style="width: 8%">Количество</th>
                <th style="width: 8%">Скидка</th>
                <th style="width: 10%">Стоимость</th>
                <th style="width: 3%"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->orderItems as $key => $orderItem): ?>
                <tr class="item exist-item">
                    <td class="order-item-image">
                        <?= Html::img('' . $orderItem->product->productImages[0]->imageUrl, [
                            'class' => 'item-image'
                        ]) ?>
                    </td>
                    <td>
                        <?= $form->field($orderItem, "[$key]id", [
                            'options' => ['tag' => null],
                            'template' => '{input}',
                        ])->hiddenInput()
                        ?>
                        <?= $form->field($orderItem, "[$key]productId", [
                            'options' => ['tag' => null],
                            'template' => '{input}',
                        ])->dropDownList(ArrayHelper::map(\admin\models\Product::find()->all(), 'id', 'name'))
                        ?>
                    </td>
                    <td>
                        <?= $form->field($orderItem, "[$key]productPrice", [
                            'options' => ['tag' => null],
                            'template' => '{input}',
                        ])->textInput([
                            'readonly' => true,
                        ]) ?>
                    </td>
                    <td>
                        <?= $form->field($orderItem, "[$key]quantity", [
                            'options' => ['tag' => null],
                            'template' => '{input}',
                        ])->input('number', ['min' => 1]) ?>
                    </td>
                    <td>
                        <?= $form->field($orderItem, "[$key]discount", [
                            'options' => ['tag' => null],
                            'template' => '{input}',
                        ])->textInput([
                            'readonly' => true,
                        ]) ?>
                    </td>
                    <td>
                        <?= $form->field($orderItem, "[$key]amount", [
                            'options' => ['tag' => null],
                            'template' => '{input}',
                        ])->textInput([
                            'readonly' => true,
                        ]) ?>
                    </td>
                    <td>
                        <?= Html::button('<span class="fa fa-times"></span>', [
                            'class' => 'btn btn-xs btn-danger btn-delete-item'
                        ]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr class="item template-item">
                <td class="order-item-image">
                    <?= Html::img('https://picsum.photos/200', [
                        'class' => 'item-image'
                    ]) ?>
                </td>
                <td>
                    <?= /*$form->field($item, '[x]productId', [
                        'options' => ['tag' => null],
                        'template' => implode('', [
                            '{input}',
                            Html::textInput('item-name', '', ['class' => 'form-control autocomplete']),
                        ]),
                    ])->hiddenInput([
                        'maxlength' => true,
                        'placeholder' => 'Начните вводить название товара'
                    ])*/
                    '' ?>

                    <?= $form->field($item, '[x]productId', [
                        'options' => ['tag' => null],
                        'template' => '{input}',
                    ])->dropDownList(ArrayHelper::map(\admin\models\Product::find()->all(), 'id', 'name'), [
                        'prompt' => 'Выберите товар'
                    ]) ?>
                </td>
                <td>
                    <?= $form->field($item, '[x]productPrice', [
                        'options' => ['tag' => null],
                        'template' => '{input}',
                    ])->textInput([
//                            'readonly' => true,
                        'value' => '0'
                    ]) ?>
                </td>
                <td>
                    <?= $form->field($item, '[x]quantity', [
                        'options' => ['tag' => null],
                        'template' => '{input}',
                    ])->input('number', ['value' => 1, 'min' => 1]) ?>
                </td>
                <td>
                    <?= $form->field($item, '[x]discount', [
                        'options' => ['tag' => null],
                        'template' => '{input}',
                    ])->textInput([
                        'maxlength' => true,
                    ]) ?>
                </td>
                <td>
                    <?= $form->field($item, '[x]amount', [
                        'options' => ['tag' => null],
                        'template' => '{input}',
                    ])->textInput([
                        'readonly' => true,
                    ]) ?>
                </td>
                <td>
                    <?= Html::button('<span class="fa fa-times"></span>', [
                        'class' => 'btn btn-xs btn-danger btn-delete-item'
                    ]) ?>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-3" style="margin-top: 1.8em">
                <?= HtmlHelpers::renderSubmitButton() ?>
            </div>
            <div class="col-sm-3 col-sm-offset-6">
                <?= $form->field($model, 'amount')->textInput([
//                    'readonly' => 'readonly'
                ])->label('Общая стоимость заказа') ?>
            </div>
        </div>
    </fieldset>
</div>
<?php ActiveForm::end(); ?>
