<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Order */
/* @var $item admin\models\OrderItem */
/* @var $statuses array */

$this->title = $model->isNewRecord ? 'Новый заказ' : "Редактирование заказа #{$model->id}";
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
?>
<div class="orders-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'item' => $item,
        'statuses' => $statuses,
    ]) ?>
</div>
