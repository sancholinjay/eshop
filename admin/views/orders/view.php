<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Order */

$this->title = "Заказ #{$model->id}";
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">
    <?= Html::a('<span class="fa fa-edit"></span> Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('<span class="fa fa-trash"></span> Архивировать', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Вы действительно хотите перенести заказ в архив??',
            'method' => 'post',
        ],
    ]) ?>
    <h1 style="margin-top: 1em"><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-sm-12">
            <fieldset>
                <legend class="h4">Детали заказа</legend>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        ['attribute' => 'userId',
                            'value' => function ($item) {
                                return $item->user->name;
                            }
                        ],
                        ['attribute' => 'status',
                            'value' => function ($item) {
                                return $item->orderStatus->name;
                            }
                        ],
                        'amount',
                        'createdAt',
                        'updatedAt',
                        'deleted:boolean',
                    ],
                ]) ?>
            </fieldset>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <fieldset>
                <legend class="h4">Адрес доставки</legend>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'addressState',
                        'addressRegion',
                        'addressLocality',
                        'addressStreet',
                        'addressHouse',
                        'addressApartment',
                        'zipCode'
                    ],
                ]) ?>
            </fieldset>
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => new ArrayDataProvider(['allModels' => $model->orderItems]),
        'rowOptions' => ['class' => 'item   '],
        'columns' => [
            [
                'attribute' => 'productId',
                'content' => function ($item) {
                    $imageObjs = $item->product->productImages;
                    if (is_array($imageObjs) && !empty($imageObjs)) {
                        $url = $imageObjs[0]->imageUrl;
                    } else {
                        $url = '';
                    }
                    return implode(' ', [
                        Html::img($url, ['class' => 'item-image']),
                        Html::tag('span', $item->product->name, ['class' => 'item-name'])
                    ]);
                }
            ],
            'productPrice',
            'quantity',
            'discount',
            [
                'attribute' => 'amount',
                'value' => function ($item) {
                    return number_format($item->productPrice * $item->quantity -
                        ($item->productPrice / 100 * $item->discount),
                        2
                    );
                }
            ]
        ]
    ]) ?>

</div>
