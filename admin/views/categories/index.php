<?php

use admin\components\ActionColumn;
use admin\components\HtmlHelpers;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории товаров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::button('Добавить категорию', [
        'class' => 'btn btn-success action-create',
        'data-url' => Url::to(['create']),
    ]) ?>

    <?php Pjax::begin(['id' => 'categories-pjax', 'timeout' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => 'Нет',
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 50px']
            ],
            'name',
            [
                'attribute' => 'parentCategory',
                'value' => 'parent.name',
            ],
            [
                'class' => ActionColumn::class,
                'headerOptions' => ['style' => 'width: 64px'],
                'renderHtmlButtons' => true,
                'visibleButtons' => [
                    'view' => false,
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    <?= HtmlHelpers::renderEditorModal() ?>
</div>
