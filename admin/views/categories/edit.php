<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $parents array */

$this->title = $model->isNewRecord ? "Новая категория" : "Изменить категорию: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Категории товаров', 'url' => ['index  ']];
?>
<div class="category-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'parents' => $parents,
    ]) ?>
</div>
