<?php

use admin\components\HtmlHelpers;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $parents array */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'category-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<?= $form->field($model, 'name')->textInput([
    'maxlength' => true,
    'autocomplete' => false,
    'autofocus' => true
]) ?>
<?= $form->field($model, 'parentId')->dropDownList($parents, ['prompt' => 'Нет']) ?>
<?= HtmlHelpers::renderSubmitButton() ?>
<?php ActiveForm::end(); ?>
