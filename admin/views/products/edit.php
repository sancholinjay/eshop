<?php

use admin\models\forms\ProductImageForm;
use admin\models\ProductCharacteristic;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $brands array */
/* @var $categories array */
/* @var $characteristics ProductCharacteristic[] */
/* @var $imageForm ProductImageForm */

$this->title = $model->isNewRecord ? 'Новый товар' : "Редактирование товара: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
?>
<div class="products-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'brands' => $brands,
        'categories' => $categories,
        'characteristics' => $characteristics,
        'imageForm' => $imageForm,
    ]) ?>
</div>
