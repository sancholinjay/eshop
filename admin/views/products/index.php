<?php

use admin\components\ActionColumn;
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $brandsExist int */
/* @var $categoriesExist int */
/* @var $characteristicExist int */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
$enableManagement = $categoriesExist && $brandsExist && $characteristicExist;
?>
<div class="products-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($enableManagement): ?>
        <?= Html::a('Добавить товар', ['create'], ['class' => 'btn btn-success']) ?>
    <?php else: ?>
        <?= Alert::widget([
            'options' => [
                'class' => 'alert-danger alert-dismissible',
            ],
            'body' => '<h4><i class="icon fa fa-ban"></i> Управление товарами недоступно</h4>' .
                join("<br>", [
                    'Для управления товарами требуется сначала добавить:',
                    '* ' . Html::a('Бренды', ['/brands']),
                    '* ' . Html::a('Категории товаров', ['/categories']),
                    '* ' . Html::a('Характеристики товаров', ['/characteristics']),
                ]),
        ]) ?>
    <?php endif; ?>
    <?php Pjax::begin(['id' => 'products-pjax', 'timeout' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['attribute' => 'id',
                'headerOptions' => ['style' => 'width: 50px']
            ],
            'name',
            'categoryId',
            'brandId',
            'description:ntext',
            'price',
            'priceOld',
            'inStock:boolean',
            'createdAt',
            'updatedAt',
            [
                'class' => ActionColumn::class,
                'headerOptions' => ['style' => 'width: 64px'],
                'visibleButtons' => [
                    'update' => $enableManagement,
                    'delete' => $enableManagement,
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
