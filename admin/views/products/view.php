<?php

use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model admin\models\Product */

$this->title = "Товар #{$model->id}: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">
    <?= Html::a('<span class="fa fa-edit"></span> Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('<span class="fa fa-trash"></span> Архивировать', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Вы действительно хотите перенести этот товар в архив?',
            'method' => 'post',
        ],
    ]) ?>

    <br/>
    <br/>
    <div class="row">
        <div class="col-sm-4">
            <div class="row">
                <div class="col-sm-12">
                    <?= Html::img($model->getImageUrl(), [
                        'class' => 'product-main-img'
                    ]) ?>
                </div>
                <div class="col-sm-12">
                    <?php foreach ($model->productImages as $key => $image) {
                        if ($key == 0) continue;
                        echo Html::img($image->imageUrl, ['style' => 'max-width:50px; max-height:50px']);
                    } ?>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <h1><?= Html::encode($model->name) ?></h1>
            <h3><?= Html::encode($model->price) ?></h3>
            <h5>
                <del><?= Html::encode($model->priceOld) ?></del>
            </h5>
            <h4><?= $model->inStock ? "В наличии" : "Нет в наличии" ?></h4>
            <h5>Бренд: <?= $model->brand->name ?></h5>
            <h5>Категория: <?= $model->category->name ?></h5>
            <h5>Дата создания: <?= date('d.m.Y', strtotime($model->createdAt)) ?></h5>
            <h5>Архивирован: <?= $model->deleted ? "Да" : "Нет" ?></h5>
        </div>
        <div class="col-sm-12">
            <fieldset>
                <legend class="h4">Характеристики товара</legend>
                <?= ListView::widget([
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => $model->productCharacteristics
                    ]),
                    'layout' => '<dl class="dl-horizontal">{items}</dl>',
                    'itemView' => function ($item) {
                        return "<dt>{$item->characteristic->name}:</dt> <dd>{$item->value}</dd>";
                    }
                ]) ?>
            </fieldset>
        </div>
        <div class="col-sm-12">
            <fieldset>
                <legend class="h4">Описание товара</legend>
                <p><?= $model->description ? $model->description : 'У товара нет описания' ?></p>
            </fieldset>
        </div>
    </div>

    <br/>
    <br/>
    <div class="">


    </div>
