<?php

use admin\components\HtmlHelpers;
use admin\models\forms\ProductImageForm;
use common\models\ProductCharacteristic;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $characteristics ProductCharacteristic[] */

?>

<?php $form = ActiveForm::begin(['id' => 'characteristic-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<div class="row">
    <div class="col-sm-8">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-2">
        <?= $form->field($model, 'price')->textInput() ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'priceOld')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'categoryId')->dropDownList($categories, [
            'prompt' => 'Выберите категорию',
        ]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'brandId')->dropDownList($brands, [
            'prompt' => 'Выберите бренд'
        ]) ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'inStock', [
            'options' => ['style' => 'margin-top:2.1em'],
        ])->checkbox(['style' => 'width: 18px; height: 18px; vertical-align: bottom']) ?>
    </div>
</div>
<style>
    .fff {
        width: 160px;
        height: 160px;
        border: 1px solid red;
        margin-right: 10px;
    }
</style>
<div class="row">
    <fieldset class="col-sm-12" style="margin-bottom: 15px">
        <legend class="h4">Изображения товара</legend>
        <div style="display: flex; flex-flow: row">
            <?php foreach ($model->productImages as $key => $image): ?>
                <div class="fff">
                    <div class="fff-overlay">
                        <img src="<?= $image->imageUrl ?>" alt="">
                        <span class="fa fa-edit"></span>
                        <span class="fa fa-trash"></span>
                    </div>
                    <?= $form->field($images[$key], [$key] . 'id')->hiddenInput()->label(false) ?>
                    <?= $form->field($images[$key], [$key] . 'image')
                        ->fileInput(['style' => 'display:none'])->label(false) ?>
                </div>
            <?php endforeach; ?>
            <div class="fff add-new">
                <div class="fff-overlay">
                    <span class="fa fa-plus"></span>
                </div>
                <?= $form->field($images[count($model->productImages)], '[0]image')
                    ->fileInput(['style' => 'display:none'])->label(false) ?>
            </div>
            <div class="fff add-new">
                <div class="fff-overlay">
                    <span class="fa fa-plus"></span>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="row">
    <fieldset class="col-sm-12" style="margin-bottom: 15px">
        <legend class="h4">Характеристики товара</legend>
        <div class="row">
            <div class="col-sm-4">
                name
            </div>
            <div class="col-sm-8">
                value
            </div>
        </div>
    </fieldset>
</div>
<?= HtmlHelpers::renderSubmitButton() ?>
<?php ActiveForm::end(); ?>

