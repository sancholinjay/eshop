<?php

use admin\assets\AppAsset;
use admin\components\HtmlHelpers;
use admin\models\forms\ProductImageForm;
use admin\models\ProductCharacteristic;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $brands array */
/* @var $categories array */
/* @var $characteristics ProductCharacteristic[] */
/* @var $imageForm ProductImageForm */

$this->registerJsFile('/js/forms/products.js', [
    'position' => View::POS_END,
    'depends' => AppAsset::class,
]);

?>

<?php $form = ActiveForm::begin(['id' => 'products-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<div class="row">
    <div class="col-sm-8">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-2">
        <?= $form->field($model, 'price')->textInput() ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'priceOld')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'categoryId')->dropDownList($categories, [
            'prompt' => 'Выберите категорию',
        ]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'brandId')->dropDownList($brands, [
            'prompt' => 'Выберите бренд'
        ]) ?>
    </div>
    <div class="col-sm-2">
        <?= $form->field($model, 'inStock', [
            'options' => ['style' => 'margin-top:2.1em'],
        ])->checkbox(['style' => 'width: 18px; height: 18px; vertical-align: bottom']) ?>
    </div>
</div>

<div class="row">
    <fieldset class="col-sm-12" style="margin-bottom: 15px">
        <legend class="h4">Изображения товара</legend>
        <div id="product-images-section" style="display: flex; flex-flow: wrap">
            <?php foreach ($model->productImages as $key => $image): ?>
                <div class="product-image-block">
                    <img src="<?= $image->imageUrl ?>" alt="">
                    <div class="image-overlay">
                        <span class="fa fa-trash btn btn-delete-image"></span>
                    </div>
                    <?= $form->field($image, "[$key]id")->hiddenInput()->label(false) ?>
                </div>
            <?php endforeach; ?>
            <?= $form->field($imageForm, 'image[x]', [
                'template' => Html::img('#') .
                    Html::beginTag('div', ['class' => 'image-overlay']) .
                    Html::tag('span', '', ['class' => 'fa fa-plus btn btn-add-image']) .
                    Html::endTag('div') .
                    '{input}{hint}{error}',
                'options' => ['class' => 'form-group product-image-block add-new']
            ])->fileInput(['style' => 'display:none'])->label(false) ?>
        </div>
    </fieldset>
</div>
<div class="row">
    <fieldset class="col-sm-12" style="margin-bottom: 15px">
        <legend class="h4">Характеристики товара
            <?= Html::button('<span class="fa fa-plus"></span> Добавить', [
                'class' => 'btn btn-sm btn-primary pull-right btn-add-characteristic',
            ]) ?>
        </legend>
        <table class="table table-striped table-condensed product-characteristics">
            <thead>
            <tr>
                <th style="width: 40%">Характеристика</th>
                <th>Значение</th>
                <th style="width:25px">Уд.</th>
            </tr>
            </thead>
            <tbody>
            <tr class="characteristics template" style="display: none">
                <td>
                    <?= $form->field($characteristics[0], '[x]characteristicId', [
                        'template' => '{input}', 'options' => ['tag' => null]
                    ])->dropDownList([], ['prompt' => 'Выберите характеристику'])->label(false) ?>
                </td>
                <td>
                    <?= $form->field($characteristics[0], '[x]value', [
                        'template' => '{input}', 'options' => ['tag' => null]
                    ])->textInput()->label(false) ?>
                </td>
                <td>
                    <?= Html::button('<span class="fa fa-times"></span>', [
                        'class' => 'btn btn-xs btn-danger btn-delete-characteristic'
                    ]) ?>
                </td>
            </tr>
            <?php foreach ($model->productCharacteristics as $key => $characteristic): ?>
                <tr class="characteristics exist">
                    <td>
                        <?= $form->field($characteristic, "[$key]id", [
                            'template' => '{input}', 'options' => ['tag' => null]
                        ])->hiddenInput() ?>
                        <?= $form->field($characteristic, "[$key]characteristicId", [
                            'template' => '{label}{input}', 'options' => ['tag' => null]
                        ])->hiddenInput()->label($characteristic->characteristic->name, [
                            'style' => 'margin-top: 5px'
                        ]) ?>
                    </td>
                    <td>
                        <?= $form->field($characteristic, "[$key]value", [
                            'template' => '{input}', 'options' => ['tag' => null]
                        ])->textInput()->label(false) ?>
                    </td>
                    <td>
                        <?= Html::button('<span class="fa fa-times">', [
                            'class' => 'btn btn-xs btn-danger btn-delete-characteristic'
                        ]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </fieldset>
</div>
<?= HtmlHelpers::renderSubmitButton() ?>
<?php ActiveForm::end(); ?>

