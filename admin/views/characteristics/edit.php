<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Characteristic */
/* @var $categories array */

$this->title = $model->isNewRecord ? "Новая характеристика" : "Изменить характеристику: {$model->name}";
$this->params['breadcrumbs'][] = ['label' => 'Характеристики товаров', 'url' => ['index']];
?>
<div class="characteristic-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>
</div>
