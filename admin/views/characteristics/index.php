<?php

use admin\components\ActionColumn;
use admin\components\HtmlHelpers;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CharacteristicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categoriesExist bool */

$this->title = 'Характеристики товаров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="characteristic-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($categoriesExist): ?>
        <?= Html::button('Добавить характеристику', [
            'class' => 'btn btn-success action-create',
            'data-url' => Url::to(['create']),
        ]) ?>
    <?php else: ?>
        <?= Alert::widget([
            'options' => [
                'class' => 'alert-danger alert-dismissible',
            ],
            'body' => '<h4><i class="icon fa fa-ban"></i> Характеристики товаров недоступны</h4>' .
                'Для управления характеристиками необходимо добавить хотя бы одну ' .
                Html::a('Категорию товаров', ['/categories']),
        ]) ?>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'characteristic-pjax', 'timeout' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 50px'],
            ],
            'name',
            [
                'attribute' => 'categoryName',
                'value' => 'category.name',
            ],
            [
                'class' => ActionColumn::class,
                'headerOptions' => ['style' => 'width: 64px'],
                'renderHtmlButtons' => true,
                'visibleButtons' => [
                    'view' => false,
                    'update' => $categoriesExist,
                    'delete' => $categoriesExist,
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    <?= HtmlHelpers::renderEditorModal() ?>
</div>
