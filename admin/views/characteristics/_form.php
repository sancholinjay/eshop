<?php

use admin\components\HtmlHelpers;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Characteristic */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories array */
?>

<?php $form = ActiveForm::begin(['id' => 'characteristic-form', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->errorSummary($model) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'categoryId')->dropDownList($categories, ['prompt' => 'Выберите категорию']) ?>
<?= HtmlHelpers::renderSubmitButton() ?>
<?php ActiveForm::end(); ?>
