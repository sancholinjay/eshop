<?php

namespace admin\assets;

use common\assets\FontAwesomeAsset;
use common\assets\OpenSansAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Main admin application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',
    ];

    public $js = [
        'js/site.js',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapPluginAsset::class,
        OpenSansAsset::class,
        FontAwesomeAsset::class,
    ];
}
