$(function () {

});

function submitEditorForm(form) {
    let formData = new FormData(form[0]);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if (!response.message) {
                $('#editor-modal').find('.modal-body').html(response);
                $('#editor-modal').find('.modal-body').find('h1').remove();
                return false;
            }
            $(document).find('#editor-modal').modal('hide');
            let container = $(document).find('.grid-view').parents('div[data-pjax-container]').first().attr('id');
            $.pjax.reload({container: `#${container}`});
        },
        error: function (xhr) {
            alert(xhr.responseText);
        }
    });
}

$('body').on('click', 'button.action-create, button.action-update', function (event) {
    event.preventDefault();
    let sel = $(this);
    $.ajax({
        url: sel.data('url'),
        success: function (response) {
            let header = $('#editor-modal').find('.modal-body').html(response).find('h1').text();
            $('#editor-modal').find('.modal-header h4').text(header);
            $('#editor-modal').find('.modal-body').find('h1').remove();
            $('#editor-modal').modal('show');
        },
        error: function (xhr) {
            alert(xhr.responseText);
        }
    });
});

$('#editor-modal').on('submit', 'form', function (event) {
    event.preventDefault();
    submitEditorForm($(this));
});

$('body').on('click', 'button.action-delete', function (event) {
    event.preventDefault();
    return;
    let sel = $(this);
    $.ajax({
        url: sel.data('url'),
        method: 'POST',
        success: function (response) {
            if (response.code == '200') {
                let container = $(this).parents('div[data-pjax-container]').first().attr('id');
                $.pjax.reload({container: `#${container}`});
            }
        }

    })
});