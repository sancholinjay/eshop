let enumeratorItem = 1;

$(document).on('click', '.btn-add-item', function (event) {
    let template = $(document).find('.items-table')
        .find('.item.template-item')
        .clone();

    template.find('input, select').each(function () {
        $(this).prop('name',
            $(this).attr('name').replace('[x]', `[x${enumeratorItem}]`)
        );
        $(this).prop('id',
            $(this).attr('id').replace('-x-', `-x${enumeratorItem}-`)
        );
    });
    template.removeClass('template-item')
        .removeAttr('style')
        .appendTo('.items-table tbody');

    enumeratorItem++;
});

$(document).on('click', '.btn-delete-item', function (event) {
    $(this).parents('tr.item').remove();
});
