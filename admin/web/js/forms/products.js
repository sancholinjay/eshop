let categoryId;
let enumeratorCharacteristic = 1;
let enumeratorProductImg = 1;

$(function () {
    categoryId = $('#product-categoryid').val();
    $('#product-categoryid').trigger('change');
});

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $(input).parent().find('img').prop("src", e.target.result);
            $(input).parent().find('.btn-add-image')
                .removeClass('fa-plus')
                .removeClass('btn-add-image')
                .addClass('fa-trash')
                .addClass('btn-delete-image');
        };
        reader.readAsDataURL(input.files[0]);
        $(input).data('changed', true);
    }
}

function cloneImgBlock(input) {
    let template = $(input).parents('.product-image-block').clone();
    template.find('input').each(function () {
        $(this).val('');
        $(this).prop('name',
            $(this).attr('name').replace(/\[x(\d+)?\]/g, `[x${enumeratorProductImg}]`)
        );
        if ($(this).attr('id')) {
            $(this).prop('id',
                $(this).attr('id').replace(/-x(\d+)?/g, `-x${enumeratorProductImg}`)
            );
        }
    });
    template.appendTo($('#product-images-section'));

    enumeratorProductImg++;
}

$(document).on('change', '#product-categoryid', function (event) {
    let sel = this;
    if ($(sel).val() == '') {
        categoryId = $(sel).val();
        return false;
    }
    let createdCharacteristicsCount = $(document)
        .find('.table.product-characteristics')
        .find('tbody tr:not(".template"):not(".exist")')
        .length;
    let proceed = true;

    if (createdCharacteristicsCount > 0) {
        proceed = confirm('Выбор новой категории удалит все введенные характеристики, исключая сохраненные. Продолжить?');
    }

    if (proceed) {
        $.ajax({
            url: '/products/get-characteristic-list',
            data: {
                categoryId: $(sel).val(),
            },
            success: function (response) {
                categoryId = $(sel).val();

                $(document).find('.table.product-characteristics')
                    .find('tbody tr:not(".template"):not(".exist")')
                    .remove();
                $('#productcharacteristic-x-characteristicid').find('option').remove();
                $.map(response, function (n) {
                    return $('<option>').val(n.id).text(n.name)
                        .appendTo('#productcharacteristic-x-characteristicid');
                });
            },
            error: function (xhr) {
                $(sel).val(categoryId);
                alert(xhr.responseText);
            }
        });
    } else {
        $(this).val(categoryId);
    }
});

$(document).on('click', '.btn-add-characteristic', function (event) {
    let template = $(document).find('.product-characteristics')
        .find('.characteristics.template')
        .clone();

    template.find('input, select').each(function () {
        $(this).prop('name',
            $(this).attr('name').replace('[x]', `[x${enumeratorCharacteristic}]`)
        );
        $(this).prop('id',
            $(this).attr('id').replace('-x-', `-x${enumeratorCharacteristic}-`)
        );
    });
    template.removeClass('template')
        .removeAttr('style')
        .appendTo('.product-characteristics tbody');

    enumeratorCharacteristic++;
});

$(document).on('click', '.btn-delete-characteristic', function (event) {
    $(this).parents('tr.characteristics').remove();
});

$(document).on('click', '.product-image-block.add-new .image-overlay', function (event) {
    $(this).parent().find('input[type="file"]').trigger('click');
});

$(document).on('change', '.product-image-block.add-new input[type="file"]', function (event) {
    if (!$(this).data('changed')) {
        cloneImgBlock(this);
    }
    readURL(this);
});


$(document).on('click', '.btn-delete-image', function (event) {
    event.stopImmediatePropagation();
    if (confirm('Удалить?')) {
        $(this).parents('.product-image-block')[0].remove();
    }
});
