<?php

?>

<!-- HEADER -->
<header>
    <!-- top Header -->
    <!--    <div id="top-header">-->
    <!--        <div class="container">-->
    <!--            <div class="pull-left">-->
    <!--                <span>Welcome to E-shop!</span>-->
    <!--            </div>-->
    <!--            <div class="pull-right">-->
    <!--                <ul class="header-top-links">-->
    <!--                    <li><a href="#">Store</a></li>-->
    <!--                    <li><a href="#">Newsletter</a></li>-->
    <!--                    <li><a href="#">FAQ</a></li>-->
    <!--                    <li class="dropdown default-dropdown">-->
    <!--                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">ENG <i class="fa fa-caret-down"></i></a>-->
    <!--                        <ul class="custom-menu">-->
    <!--                            <li><a href="#">English (ENG)</a></li>-->
    <!--                            <li><a href="#">Russian (Ru)</a></li>-->
    <!--                            <li><a href="#">French (FR)</a></li>-->
    <!--                            <li><a href="#">Spanish (Es)</a></li>-->
    <!--                        </ul>-->
    <!--                    </li>-->
    <!--                    <li class="dropdown default-dropdown">-->
    <!--                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">USD <i class="fa fa-caret-down"></i></a>-->
    <!--                        <ul class="custom-menu">-->
    <!--                            <li><a href="#">USD ($)</a></li>-->
    <!--                            <li><a href="#">EUR (€)</a></li>-->
    <!--                        </ul>-->
    <!--                    </li>-->
    <!--                </ul>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!-- /top Header -->

    <!-- header -->
    <div id="header" class="container">
        <div class="row row-space-0">
            <div class="col-xs-4 col-sm-2">
                <!-- Logo -->
                <div class="header-logo">
                    <a class="logo" href="/">
                        <img src="/img/logo.png" alt="">
                    </a>
                </div>
                <!-- /Logo -->

            </div>
            <div class="col-md-6">

                <!-- Search -->
                <div class="header-search">
                    <form>
                        <input class="input search-input" type="text" placeholder="Поиск...">
                        <select class="input search-categories">
                            <option value="0">Все категории</option>
                            <option value="1">Category 01</option>
                            <option value="1">Category 02</option>
                        </select>
                        <button class="search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- /Search -->
            </div>

            <div class="col-md-4">
                <ul class="header-btns pull-right">
                    <!-- Account -->
                    <li class="header-account dropdown default-dropdown">
                        <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                            <div class="header-btns-icon">
                                <i class="fa fa-user-o"></i>
                            </div>
                            <strong class="text-uppercase">Мой аккаунт <i class="fa fa-caret-down"></i></strong>
                        </div>
                        <a href="#" class="text-uppercase text-ellipsis" style="max-width: 100px">Войти</a>
                        <ul class="custom-menu">
                            <li><a href="#"><i class="fa fa-user-o"></i> Мой профиль</a></li>
                            <li><a href="#"><i class="fa fa-exchange"></i> Мои заказы</a></li>
                            <li><a href="#"><i class="fa fa-heart-o"></i> Список желаний</a></li>
                            <li><a href="#"><i class="fa fa-check"></i> Отзывы</a></li>
                            <li><a href="#"><i class="fa fa-sign-in"></i> Войти</a></li>
                            <li><a href="#"><i class="fa fa-user-plus"></i> Регистрация</a></li>
                        </ul>
                    </li>
                    <!-- /Account -->

                    <!-- Cart -->
                    <li class="header-cart dropdown default-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <div class="header-btns-icon">
                                <i class="fa fa-2x fa-shopping-cart" style="padding-top: 6px"></i>
                                <span class="qty">0</span>
                            </div>
                            <strong class="text-uppercase">Корзина</strong>
                            <br>
                            <span class="text-nowrap d-block">0.00 руб</span>
                        </a>
                        <div class="custom-menu shopping-cart-menu">
                            <div id="shopping-cart">
                                <div class="shopping-cart-list">
                                    <div class="product product-widget">
                                        <div class="product-thumb">
                                            <img src="/img/thumb-product01.jpg" alt="">
                                        </div>
                                        <div class="product-body">
                                            <h3 class="product-price">$32.50 <span class="qty">x3</span></h3>
                                            <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                                        </div>
                                        <button class="cancel-btn"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <div class="product product-widget">
                                        <div class="product-thumb">
                                            <img src="/img/thumb-product01.jpg" alt="">
                                        </div>
                                        <div class="product-body">
                                            <h3 class="product-price">$32.50 <span class="qty">x3</span></h3>
                                            <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                                        </div>
                                        <button class="cancel-btn"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button class="btn btn-default" style="width: 40%">Просмотр</button>
                                    <button class="btn btn-primary" style="width: 60%">
                                        Оформить заказ <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- /Cart -->

                    <!-- Mobile nav toggle-->
                    <li class="nav-toggle">
                        <button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
                    </li>
                    <!-- / Mobile nav toggle -->
                </ul>
            </div>
        </div>
    </div>
    <!-- header -->
</header>
<!-- /HEADER -->
