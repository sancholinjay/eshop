<?php

use yii\helpers\Html; ?>

<!-- FOOTER -->
<footer id="footer" class="section section-grey">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <!-- footer logo -->
                    <div class="footer-logo">
                        <a class="logo" href="#">
                            <img src="/img/logo.png" alt="">
                        </a>
                    </div>
                    <!-- /footer logo -->

                    <p>г. Шаблонный, ул. Шаблонная 1/1</p>
                    <p>Тел.: +1 234 5678901</p>
                    <p>E-mail: e-shop@e-shop.domain</p>

                    <!-- footer social -->
                    <ul class="footer-social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-telegram"></i></a></li>
                    </ul>
                    <!-- /footer social -->
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Мой аккаунт</h3>
                    <ul class="list-links">
                        <li><a href="#">Профиль</a></li>
                        <li><a href="#">Спижок желаний</a></li>
                        <li><a href="#">Мои заказы</a></li>
                        <li><a href="#">Корзина</a></li>
                        <li><a href="#">Выход</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <div class="clearfix visible-sm visible-xs"></div>

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Поддержка</h3>
                    <ul class="list-links">
                        <li><a href="#">О магазине</a></li>
                        <li><a href="#">Доставка и оплата</a></li>
                        <li><a href="#">Акции</a></li>
                        <li><a href="#">Вопросы и ответы</a></li>
                        <li><a href="#">Обратная связь</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer subscribe -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Будьте в курсе</h3>
                    <p>Подпишитесь на наши новости, чтобы в числе первых узнавать об акциях и обновениях.</p>
                    <form>
                        <div class="form-group">
                            <input class="form-control" placeholder="Ваш e-mail">
                        </div>
                        <button class="btn btn-primary btn-block">
                            <span class="fa fa-envelope-o"> </span>
                            Подписаться
                        </button>
                    </form>
                </div>
            </div>
            <!-- /footer subscribe -->
        </div>
        <!-- /row -->
        <hr>
        <!-- row -->
        <div class="row">
            <div class="col-md-6">
                <div class="footer-copyright">
                    Copyright <?= date('Y') ?> <?= Yii::$app->name ?>
                </div>
            </div>
            <div class="col-md-6 text-right">
                <div class="footer-copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Дизайн: <a href="https://colorlib.com" target="_blank">Colorlib</a>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</footer>
<!-- /FOOTER -->
