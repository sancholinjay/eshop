<?php

namespace shop\models;

use Yii;

/**
 * This is the model class for table "User".
 *
 * @property int $id
 * @property string $username Логин
 * @property string $email E-mail
 * @property string $passwordHash Пароль (хэш)
 * @property string $passwordResetToken Код восстановления пароля
 * @property string $verificationToken Код верификации
 * @property string $authKey Ключ аутентификации
 * @property string $lastName Фамилия
 * @property string $firstName Имя
 * @property int $phoneNumber Номер телефона
 * @property string $profileImgUrl Аватар
 * @property int $status Статус
 * @property string $createdAt Дата создания
 * @property string $updatedAt Дата изменения
 *
 * @property Order[] $orders
 */
class User extends \common\models\User
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'passwordHash', 'authKey', 'lastName', 'firstName', 'createdAt', 'updatedAt'], 'required'],
            [['phoneNumber', 'status'], 'integer'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['username', 'email', 'passwordHash', 'passwordResetToken', 'verificationToken', 'lastName', 'firstName', 'profileImgUrl'], 'string', 'max' => 255],
            [['authKey'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['passwordResetToken'], 'unique'],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'email' => 'E-mail',
            'passwordHash' => 'Пароль (хэш)',
            'passwordResetToken' => 'Код восстановления пароля',
            'verificationToken' => 'Код верификации',
            'authKey' => 'Ключ аутентификации',
            'lastName' => 'Фамилия',
            'firstName' => 'Имя',
            'phoneNumber' => 'Номер телефона',
            'profileImgUrl' => 'Аватар',
            'status' => 'Статус',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['userId' => 'id']);
    }
}
