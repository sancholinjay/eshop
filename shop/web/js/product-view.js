// PRODUCT PIC SQUARE BLOCK
function squareBlock() {
    let blockWidth = $('#product-main-view').width;
    $('#product-main-view').height = blockWidth;
}


// PRODUCT DETAILS SLICK
// $('#product-main-view').slick({
//     infinite: true,
//     speed: 300,
//     dots: false,
//     arrows: true,
//     fade: true,
//     asNavFor: '#product-view',
// });

$('#product-view').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    centerMode: true,
    focusOnSelect: true,
    // asNavFor: '#product-main-view',
});

$('#product-view').on('afterChange', function (event, slick, currentSlide) {
    let url = $(slick.$slides[currentSlide]).find('img').attr('src');
    $('#product-main-view img:first').prop('src', url);
    setTimeout(function () {
        $('#product-main-view .product-view').trigger('zoom.destroy').zoom({url: url});
    }, 50);
});

// PRODUCT ZOOM
$('#product-main-view .product-view').zoom();

$(document).ready(function (event) {
    squareBlock();
});

$(window).on('resize', function (event) {
    squareBlock();
});

