<?php

namespace shop\controllers;

use common\models\Product;
use yii\web\NotFoundHttpException;

class ProductController extends _BaseController
{

    public function actionIndex()
    {
        return $this->redirect(['/catalog/']);
    }

    public function actionView($id)
    {
        $model = Product::findOne(['id' => $id, 'deleted' => false]);
        if (!$model) {
            throw new NotFoundHttpException("Товар недоступен или не сущетсвует");
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
