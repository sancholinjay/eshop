<?php

namespace shop\assets;

use common\assets\FontAwesomeAsset;
use common\assets\OpenSansAsset;
use common\assets\SlickCarouselAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Main shop application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/style.css',
//        'css/site-responsive.css',
        'css/site.css',
    ];

    public $js = [
//        'js/site.js',
        'js/main.js',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapPluginAsset::class,
        OpenSansAsset::class,
        FontAwesomeAsset::class,
        SlickCarouselAsset::class,
    ];
}
