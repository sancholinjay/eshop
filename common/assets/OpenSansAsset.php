<?php

namespace common\assets;

use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;

class OpenSansAsset extends AssetBundle
{
    public $sourcePath = '@bower/open-sans-fontface';

    public $css = [
        'open-sans.css',
    ];

    public $depends = [
        BootstrapPluginAsset::class,
    ];
}
