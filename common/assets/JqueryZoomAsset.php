<?php

namespace common\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class JqueryZoomAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-zoom';

    public $js = [
        'jquery.zoom.min.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
