<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductReview".
 *
 * @property int $id
 * @property int $productId Товар
 * @property string $username Имя пользователя
 * @property string $email E-mail
 * @property int $rating Оценка
 * @property string $comment Комментарий
 * @property bool $approved Опубликован
 * @property string $createdAt Дата добавления
 * @property bool $deleted Архивирован
 *
 * @property Product $product
 */
class ProductReview extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ProductReview';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['productId', 'username', 'email'], 'required'],
            [['productId', 'rating'], 'integer'],
            [['approved', 'deleted'], 'boolean'],
            [['createdAt'], 'safe'],
            [['username', 'email'], 'string', 'max' => 64],
            [['comment'], 'string', 'max' => 512],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Товар',
            'username' => 'Имя пользователя',
            'email' => 'E-mail',
            'rating' => 'Оценка',
            'comment' => 'Комментарий',
            'approved' => 'Опубликован',
            'createdAt' => 'Дата добавления',
            'deleted' => 'Архивирован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }
}
