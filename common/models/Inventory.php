<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Inventory".
 *
 * @property int $id
 * @property int $productId Товар
 * @property int $quantity Количество
 * @property int $reserved Резерв
 * @property int $stock Доступно
 * @property string $createdAt Дата получения
 * @property string $updatedAt Дата изменения
 *
 * @property Product $product
 */
class Inventory extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Inventory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['productId', 'updatedAt'], 'required'],
            [['productId', 'quantity', 'reserved', 'stock'], 'integer'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['productId'], 'unique'],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Товар',
            'quantity' => 'Количество',
            'reserved' => 'Резерв',
            'stock' => 'Доступно',
            'createdAt' => 'Дата получения',
            'updatedAt' => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }
}
