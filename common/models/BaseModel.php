<?php
/** @noinspection PropertiesInspection */
/** @noinspection UndetectableTableInspection */

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * @property string createdAt
 * @property string updatedAt
 * @property bool deleted
 */
class BaseModel extends ActiveRecord
{
    public function delete()
    {
        if ($this->hasAttribute('deleted')) {
            $this->deleted = true;
            if (!$this->save()) {
                throw new Exception('Can not mark the model as deleted', Json::encode($this->errors));
            }
            return true;
        } else {
            return parent::delete();
        }
    }

}
