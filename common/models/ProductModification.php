<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductModification".
 *
 * @property int $id
 * @property int $productId Товар
 * @property int $modificationId Модификация
 * @property string $value Значение
 *
 * @property Modification $modification
 * @property Product $product
 */
class ProductModification extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ProductModification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['productId', 'modificationId', 'value'], 'required'],
            [['productId', 'modificationId'], 'default', 'value' => null],
            [['productId', 'modificationId'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['productId', 'modificationId', 'value'], 'unique', 'targetAttribute' => ['productId', 'modificationId', 'value']],
            [['modificationId'], 'exist', 'skipOnError' => true, 'targetClass' => Modification::className(), 'targetAttribute' => ['modificationId' => 'id']],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Товар',
            'modificationId' => 'Модификация',
            'value' => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModification()
    {
        return $this->hasOne(Modification::className(), ['id' => 'modificationId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }
}
