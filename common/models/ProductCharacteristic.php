<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductCharacteristic".
 *
 * @property int $id
 * @property int $productId Товар
 * @property int $characteristicId Характеристика
 * @property string $value Значение
 *
 * @property Characteristic $characteristic
 * @property Product $product
 */
class ProductCharacteristic extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ProductCharacteristic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['productId', 'characteristicId', 'value'], 'required'],
            [['productId', 'characteristicId'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['productId', 'characteristicId', 'value'], 'unique', 'targetAttribute' => ['productId', 'characteristicId', 'value']],
            [['characteristicId'], 'exist', 'skipOnError' => true, 'targetClass' => Characteristic::className(), 'targetAttribute' => ['characteristicId' => 'id']],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Товар',
            'characteristicId' => 'Характеристика',
            'value' => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristic()
    {
        return $this->hasOne(Characteristic::className(), ['id' => 'characteristicId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }
}
