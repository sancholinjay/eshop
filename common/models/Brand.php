<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Brand".
 *
 * @property int $id
 * @property string $name Наименование
 * @property bool $deleted Архивирован
 *
 * @property Product[] $products
 */
class Brand extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'boolean'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'deleted' => 'Архивирован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['brandId' => 'id']);
    }
}
