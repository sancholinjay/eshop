<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Characteristic".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $categoryId Категория
 * @property bool $deleted Архивирована
 *
 * @property Category $category
 * @property ProductCharacteristic[] $productCharacteristics
 */
class Characteristic extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Characteristic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'categoryId'], 'required'],
            [['categoryId'], 'integer'],
            [['deleted'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'categoryId' => 'Категория',
            'deleted' => 'Архивирована',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCharacteristics()
    {
        return $this->hasMany(ProductCharacteristic::className(), ['characteristicId' => 'id']);
    }
}
