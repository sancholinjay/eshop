<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductImage".
 *
 * @property int $id
 * @property int $productId Товар
 * @property string $imageUrl Картинка
 *
 * @property Product $product
 */
class ProductImage extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ProductImage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['productId', 'imageUrl'], 'required'],
            [['productId'], 'integer'],
            [['imageUrl'], 'string', 'max' => 255],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Товар',
            'imageUrl' => 'Картинка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }
}
