<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Order".
 *
 * @property int $id Номер заказа
 * @property int $userId Покупатель
 * @property int $status Статус
 * @property string $amount Стоимость
 * @property string $addressState Область
 * @property string $addressRegion Район
 * @property string $addressLocality Нас. пункт
 * @property string $addressStreet Улица
 * @property string $addressHouse Дом
 * @property string $addressApartment Квартира
 * @property string $zipCode Индекс
 * @property string $createdAt Дата создания
 * @property string $updatedAt Дата изменения
 * @property bool $deleted Архивирован
 *
 * @property OrderStatus $orderStatus
 * @property User $user
 * @property OrderItem[] $orderItems
 */
class Order extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'amount', 'status', 'addressLocality', 'addressStreet', 'addressHouse', 'updatedAt'], 'required'],
            [['userId', 'status'], 'integer'],
            [['amount'], 'number'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['deleted'], 'boolean'],
            [['addressState', 'addressRegion', 'addressLocality', 'addressStreet'], 'string', 'max' => 64],
            [['addressHouse'], 'string', 'max' => 5],
            [['addressApartment'], 'string', 'max' => 4],
            [['zipCode'], 'string', 'max' => 10],
            [['status'], 'default', 'value' => 1],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['status' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'userId' => 'Покупатель',
            'status' => 'Статус',
            'amount' => 'Общая стоимость',
            'addressState' => 'Область',
            'addressRegion' => 'Район',
            'addressLocality' => 'Нас. пункт',
            'addressStreet' => 'Улица',
            'addressHouse' => 'Дом',
            'addressApartment' => 'Квартира',
            'zipCode' => 'Индекс',
            'createdAt' => 'Дата оформления',
            'updatedAt' => 'Дата изменения',
            'deleted' => 'Архивирован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['orderId' => 'id']);
    }
}
