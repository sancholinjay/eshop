<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Product".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $categoryId Категория
 * @property int $brandId Бренд
 * @property string $code Код товара
 * @property string $article Артикул
 * @property string $description Описание
 * @property string $price Цена
 * @property string $priceOld Старая цена
 * @property bool $inStock В наличии
 * @property string $createdAt Дата создания
 * @property string $updatedAt Дата изменения
 * @property bool $deleted Архивирован
 *
 * @property Inventory $inventory
 * @property OrderItem[] $orderItems
 * @property Brand $brand
 * @property Category $category
 * @property ProductCharacteristic[] $productCharacteristics
 * @property ProductImage[] $productImages
 * @property ProductModification[] $productModifications
 * @property ProductReview[] $productReviews
 */
class Product extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'categoryId', 'brandId', 'price', 'updatedAt'], 'required'],
            [['categoryId', 'brandId'], 'integer'],
            [['description'], 'string'],
            [['price', 'priceOld'], 'number'],
            [['inStock', 'deleted'], 'boolean'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 16],
            [['article'], 'string', 'max' => 24],
            [['brandId'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brandId' => 'id']],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'categoryId' => 'Категория',
            'brandId' => 'Бренд',
            'code' => 'Код товара',
            'article' => 'Артикул',
            'description' => 'Описание',
            'price' => 'Цена',
            'priceOld' => 'Старая цена',
            'inStock' => 'В наличии',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата изменения',
            'deleted' => 'Архивирован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(Inventory::className(), ['productId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['productId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brandId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCharacteristics()
    {
        return $this->hasMany(ProductCharacteristic::className(), ['productId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['productId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductModifications()
    {
        return $this->hasMany(ProductModification::className(), ['productId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductReviews()
    {
        return $this->hasMany(ProductReview::className(), ['productId' => 'id']);
    }
}
