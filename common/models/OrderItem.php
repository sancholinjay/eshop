<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "OrderItem".
 *
 * @property int $id
 * @property int $orderId Номер заказа
 * @property int $productId Товар
 * @property int $quantity Количество
 * @property string $productPrice Цена за единицу
 * @property string $discount Скидка
 * @property bool $deleted Архивирован
 *
 * @property Order $order
 * @property Product $product
 */
class OrderItem extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OrderItem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orderId', 'productId', 'productPrice'], 'required'],
            [['orderId', 'productId', 'quantity'], 'integer'],
            [['productPrice', 'discount'], 'number'],
            [['deleted'], 'boolean'],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['orderId' => 'id']],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderId' => 'Номер заказа',
            'productId' => 'Товар',
            'quantity' => 'Количество',
            'productPrice' => 'Цена за единицу',
            'discount' => 'Скидка',
            'deleted' => 'Архивирован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'orderId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }
}
