<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "OrderStatus".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $color Цвет
 * @property string $description Описание
 * @property bool $deleted Архивирован
 *
 * @property Order[] $orders
 */
class OrderStatus extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OrderStatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'boolean'],
            [['name', 'description'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 9],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'color' => 'Цвет',
            'description' => 'Описание',
            'deleted' => 'Архивирован',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['status' => 'id']);
    }
}
