<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Modification".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $categoryId Категория
 * @property int $dataType Тип данных
 * @property bool $deleted Архивирована
 *
 * @property Category $category
 * @property ProductModification[] $productModifications
 */
class Modification extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Modification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'categoryId', 'dataType'], 'required'],
            [['categoryId', 'dataType'], 'integer'],
            [['deleted'], 'boolean'],
            [['name'], 'string', 'max' => 64],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'categoryId' => 'Категория',
            'dataType' => 'Тип данных',
            'deleted' => 'Архивирована',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductModifications()
    {
        return $this->hasMany(ProductModification::className(), ['modificationId' => 'id']);
    }
}
