<?php
return [
    'language' => 'ru-RU',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => ['log'],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
//        'log' => [
//            'traceLevel' => 0,
//            'targets' => [
//                [
//                    'class' => 'common\components\log\PostgresTarget',
//                    'levels' => ['error', 'warning'],
////                    'levels' => ['error'],
//                    'logVars' => [],
//                ],
//            ],
//
//        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
        ]
    ],
];
