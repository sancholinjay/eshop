<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@shop', dirname(dirname(__DIR__)) . '/shop');
Yii::setAlias('@admin', dirname(dirname(__DIR__)) . '/admin');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
