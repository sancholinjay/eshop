<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m190806_112304_add_Inventory
 */
class m190806_112304_add_Inventory extends Migration
{
    private $tableName = '{{%Inventory}}';
    private $productTable = '{{%Product}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'productId' => $this->integer()->notNull()->unique()->comment('Товар'),
            'quantity' => $this->integer()->notNull()->defaultValue(1)->comment('Количество'),
            'reserved' => $this->integer()->notNull()->defaultValue(1)->comment('Резерв'),
            'stock' => $this->integer()->notNull()->defaultValue(1)->comment('Доступно'),

            'createdAt' => $this->dateTime()->notNull()->defaultValue(new Expression('now()'))
                ->comment('Дата получения'),
            'updatedAt' => $this->dateTime()->notNull()->comment('Дата изменения'),
        ]);

        $this->addForeignKey('Inventory_productId_fk',
            $this->tableName,
            'productId',
            $this->productTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('Inventory_productId_fk', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
