<?php

use yii\db\Migration;

/**
 * Class m190805_171605_add_Category
 */
class m190805_171605_add_Category extends Migration
{
    private $tableName = '{{%Category}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'parentId' => $this->integer()->notNull()->defaultValue(0)->comment('Родительская категория'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирована'),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
