<?php

use yii\db\Migration;

/**
 * Class m190903_180358_add_Modification
 */
class m190903_180358_add_Modification extends Migration
{
    private $tableName = '{{%Modification}}';
    private $categoriesTable = '{{%Category}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull()->comment('Наименование'),
            'categoryId' => $this->integer()->notNull()->comment('Категория'),
            'dataType' => $this->integer()->notNull()->comment('Тип данных'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирована'),
        ]);

        $this->addForeignKey('Modification_categoryId_fk',
            $this->tableName,
            'categoryId',
            $this->categoriesTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('Modification_categoryId_fk', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
