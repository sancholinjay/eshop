<?php

use yii\db\Expression;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    private $tableName = '{{%User}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique()->comment('Логин'),
            'email' => $this->string()->notNull()->unique()->comment('E-mail'),
            'passwordHash' => $this->string()->notNull()->comment('Пароль (хэш)'),
            'passwordResetToken' => $this->string()->unique()->comment('Код восстановления пароля'),
            'verificationToken' => $this->string()->defaultValue(null)->comment('Код верификации'),
            'authKey' => $this->string(32)->notNull()->comment('Ключ аутентификации'),
            'lastName' => $this->string()->notNull()->comment('Фамилия'),
            'firstName' => $this->string()->notNull()->comment('Имя'),
            'phoneNumber' => $this->bigInteger()->comment('Номер телефона'),
            'profileImgUrl' => $this->string()->comment('Аватар'),

            'status' => $this->smallInteger()->notNull()->defaultValue(10)->comment('Статус'),
            'createdAt' => $this->dateTime()->notNull()->defaultValue(new Expression('now()'))
                ->comment('Дата создания'),
            'updatedAt' => $this->dateTime()->notNull()->comment('Дата изменения'),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
