<?php

use yii\db\Migration;

/**
 * Class m190805_174606_add_ProductImage
 */
class m190805_174606_add_ProductImage extends Migration
{
    private $tableName = '{{%ProductImage}}';
    private $productTable = '{{%Product}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'productId' => $this->integer()->notNull()->comment('Товар'),
            'imageUrl' => $this->string()->notNull()->comment('Картинка'),
        ]);

        $this->addForeignKey('ProductImage_productId_fk',
            $this->tableName,
            'productId',
            $this->productTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('ProductImage_productId_fk', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
