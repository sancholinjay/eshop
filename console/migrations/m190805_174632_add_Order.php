<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m190805_174632_add_Order
 */
class m190805_174632_add_Order extends Migration
{
    private $tableName = '{{%Order}}';
    private $userTable = '{{%User}}';
    private $statusTable = '{{%OrderStatus}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('Номер заказа'),
            'userId' => $this->integer()->notNull()->comment('Покупатель'),
            'status' => $this->integer()->notNull()->defaultValue(1)->comment('Статус'),
            'amount' => $this->money(10, 2)->notNull()->comment('Общая стоимость'),

            'addressState' => $this->string(64)->comment('Область'),
            'addressRegion' => $this->string(64)->comment('Район'),
            'addressLocality' => $this->string(64)->notNull()->comment('Нас. пункт'),
            'addressStreet' => $this->string(64)->notNull()->comment('Улица'),
            'addressHouse' => $this->string(5)->notNull()->comment('Дом'),
            'addressApartment' => $this->string(4)->comment('Квартира'),
            'zipCode' => $this->string(10)->comment('Индекс'),

            'createdAt' => $this->dateTime()->notNull()->defaultValue(new Expression('now()'))
                ->comment('Дата оформления'),
            'updatedAt' => $this->dateTime()->notNull()->comment('Дата изменения'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирован'),
        ]);

        $this->addForeignKey('Order_userId_fk',
            $this->tableName,
            'userId',
            $this->userTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey('Order_status_fk',
            $this->tableName,
            'status',
            $this->statusTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('Order_userId_fk', $this->tableName);
        $this->dropForeignKey('Order_status_fk', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
