<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m190805_174305_add_Product
 */
class m190805_174305_add_Product extends Migration
{
    private $tableName = '{{%Product}}';
    private $brandsTable = '{{%Brand}}';
    private $categoriesTable = '{{%Category}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'categoryId' => $this->integer()->notNull()->comment('Категория'),
            'brandId' => $this->integer()->notNull()->comment('Бренд'),
            'code' => $this->string(16)->comment('Код товара'),
            'article' => $this->string(24)->comment('Артикул'),
            'description' => $this->text()->comment('Описание'),
            'price' => $this->money(10, 2)->notNull()->comment('Цена'),
            'priceOld' => $this->money(10, 2)->comment('Старая цена'),
            'inStock' => $this->boolean()->notNull()->defaultValue(true)->comment('В наличии'),

            'createdAt' => $this->dateTime()->notNull()->defaultValue(new Expression('now()'))
                ->comment('Дата создания'),
            'updatedAt' => $this->dateTime()->notNull()->comment('Дата изменения'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирован'),
        ]);

        $this->addForeignKey('Product_brandId_fk',
            $this->tableName,
            'brandId',
            $this->brandsTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey('Product_categoryId_fk',
            $this->tableName,
            'categoryId',
            $this->categoriesTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('Product_brandId_fk', $this->tableName);
        $this->dropForeignKey('Product_categoryId_fk', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
