<?php

use yii\db\Migration;

/**
 * Class m190903_180416_add_ProductModification
 */
class m190903_180416_add_ProductModification extends Migration
{
    private $tableName = '{{%ProductModification}}';
    private $productTable = '{{%Product}}';
    private $modificationTable = '{{%Modification}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'productId' => $this->integer()->notNull()->comment('Товар'),
            'modificationId' => $this->integer()->notNull()->comment('Модификация'),
            'value' => $this->string()->notNull()->comment('Значение'),
        ]);

        $this->addForeignKey('ProductModification_productId_fk',
            $this->tableName,
            'productId',
            $this->productTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey('ProductModification_modificationId_fk',
            $this->tableName,
            'modificationId',
            $this->modificationTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->createIndex('PM_productId_modificationId_value_idx',
            $this->tableName, [
                'productId', 'modificationId', 'value'
            ], true
        );
    }

    public function safeDown()
    {
        $this->dropIndex('PM_productId_modificationId_value_idx', $this->tableName);
        $this->dropForeignKey('ProductModification_productId_fk', $this->tableName);
        $this->dropForeignKey('ProductModification_modificationId_fk', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
