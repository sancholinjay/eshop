<?php

use yii\db\Migration;

/**
 * Class m190805_171618_add_Brand
 */
class m190805_171618_add_Brand extends Migration
{
    private $tableName = '{{%Brand}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирован'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
