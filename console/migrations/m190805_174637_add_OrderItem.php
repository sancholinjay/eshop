<?php

use yii\db\Migration;

/**
 * Class m190805_174637_add_OrderItem
 */
class m190805_174637_add_OrderItem extends Migration
{
    private $tableName = '{{%OrderItem}}';
    private $orderTable = '{{%Order}}';
    private $productTable = '{{%Product}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'orderId' => $this->integer()->notNull()->comment('Номер заказа'),
            'productId' => $this->integer()->notNull()->comment('Товар'),
            'quantity' => $this->integer()->notNull()->defaultValue(1)->comment('Количество'),
            'productPrice' => $this->money(10,2)->notNull()->comment('Цена за единицу'),
            'discount' => $this->money(10, 2)->notNull()->defaultValue(0)->comment('Скидка'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирован'),
        ]);

        $this->addForeignKey('OrderItem_orderId_fk',
            $this->tableName,
            'orderId',
            $this->orderTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey('OrderItem_productId_fk',
            $this->tableName,
            'productId',
            $this->productTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('OrderItem_orderId_fk', $this->tableName);
        $this->dropForeignKey('OrderItem_productId_fk', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
