<?php

use yii\db\Migration;

/**
 * Class m190805_174256_add_Characteristic
 */
class m190805_174256_add_Characteristic extends Migration
{
    private $tableName = '{{%Characteristic}}';
    private $categoriesTable = '{{%Category}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull()->comment('Наименование'),
            'categoryId' => $this->integer()->notNull()->comment('Категория'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирована'),
        ]);

        $this->addForeignKey('Characteristic_categoryId_fk',
            $this->tableName,
            'categoryId',
            $this->categoriesTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('Characteristic_categoryId_fk', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
