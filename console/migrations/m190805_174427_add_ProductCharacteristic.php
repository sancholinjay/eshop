<?php

use yii\db\Migration;

/**
 * Class m190805_174427_add_ProductCharacteristic
 */
class m190805_174427_add_ProductCharacteristic extends Migration
{
    private $tableName = '{{%ProductCharacteristic}}';
    private $productTable = '{{%Product}}';
    private $characteristicTable = '{{%Characteristic}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'productId' => $this->integer()->notNull()->comment('Товар'),
            'characteristicId' => $this->integer()->notNull()->comment('Характеристика'),
            'value' => $this->string()->notNull()->comment('Значение'),
        ]);

        $this->addForeignKey('ProductCharacteristic_productId_fk',
            $this->tableName,
            'productId',
            $this->productTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey('ProductCharacteristic_characteristicId_fk',
            $this->tableName,
            'characteristicId',
            $this->characteristicTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->createIndex('PC_productId_characteristicId_value_idx',
            $this->tableName, [
                'productId', 'characteristicId', 'value'
            ], true
        );
    }

    public function safeDown()
    {
        $this->dropIndex('PC_productId_characteristicId_value_idx', $this->tableName);
        $this->dropForeignKey('ProductCharacteristic_productId_fk', $this->tableName);
        $this->dropForeignKey('ProductCharacteristic_characteristicId_fk', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
