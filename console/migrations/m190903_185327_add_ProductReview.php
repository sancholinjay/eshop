<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m190903_185327_add_ProductReview
 */
class m190903_185327_add_ProductReview extends Migration
{
    private $tableName = '{{%ProductReview}}';
    private $productTable = '{{%Product}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'productId' => $this->integer()->notNull()->comment('Товар'),
            'username' => $this->string(64)->notNull()->comment('Имя пользователя'),
            'email' => $this->string(64)->notNull()->comment('E-mail'),
            'rating' => $this->smallInteger()->notNull()->defaultValue(0)->comment('Оценка'),
            'comment' => $this->string(512)->comment('Комментарий'),
            'approved' => $this->boolean()->notNull()->defaultValue(false)->comment('Опубликован'),
            'createdAt' => $this->dateTime()->notNull()->defaultValue(new Expression('now()'))
                ->comment('Дата добавления'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Архивирован'),
        ]);

        $this->addForeignKey('ProductReview_productId_fk',
            $this->tableName,
            'productId',
            $this->productTable,
            'id',
            'RESTRICT',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('ProductReview_productId_fk', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
