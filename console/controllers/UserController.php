<?php

namespace console\controllers;

use shop\models\User;
use yii\console\Controller;
use yii\helpers\Console;

class UserController extends Controller
{
    public function actionCreate($firstName, $lastName, $username, $email, $password)
    {
        if ($user = User::findOne(['username' => $username]) === null) {
            $user = new User([
                'username' => $username,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'email' => $email,
                'status' => User::STATUS_ACTIVE,
                'createdAt' => date('Y-m-d H:i:s'),
                'updatedAt' => date('Y-m-d H:i:s'),
            ]);
        } else {
            Console::output("User {$username} already exits");
            return;
        }
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        if (!$user->save()) {
            Console::error(Console::errorSummary($user));
        } else {
            Console::output("User '{$username}' created successfully");
        }
    }

}